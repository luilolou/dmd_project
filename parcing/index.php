<?php
error_reporting(0);
define('BASE_PATH', "/Users/natalachilikina/www/dmd_project/");

require_once BASE_PATH.'classes/log/Log.php';
require_once BASE_PATH.'classes/entities/Core.php';
require_once BASE_PATH.'classes/entities/Author.php';
require_once BASE_PATH.'classes/entities/References.php';
require_once BASE_PATH.'classes/entities/Article.php';
require_once BASE_PATH.'classes/entities/Venue.php';
require_once BASE_PATH.'classes/entities/Write.php';

use classes\Log;
use classes\Author;
use classes\Article;
use classes\References;
use classes\Venue;
use classes\Write;

$dbVenue = new Venue;
$dbAuthor = new Author;
$dbWrite = new Write;
$dbArticle = new Article;

$file = './DBLP/publications.txt';

echo '<pre>';
$file_handle = fopen($file, "r");
$i = 0;
while (!feof($file_handle)) {
    $line = fgets($file_handle);
    $marker = mb_substr($line, 0, 2);
    switch($marker){
        case '#*':
            $title = trim(mb_substr($line, 2));
            break;
        case '#@':
            $aid = trim(mb_substr($line, 2));
            $atmp = explode(',', $aid);
            foreach($atmp as $author) {
                $authortmp = $dbAuthor->getResultsByName($author);
                $authors[] = $authortmp['author_id'];
            }
            break;
        case '#t':
            $year = trim(mb_substr($line, 2));
            break;
        case '#c':
            $vid = trim(mb_substr($line, 2));
            $vtmp = $dbVenue->getResultsByName($vid);
            $venue = $vtmp['venue_id'];
            break;
        case '#i':
            $index = trim(mb_substr($line, 6));
            break;
        case '#%':
            $ref[] = trim(mb_substr($line, 2));
            break;
        case '#!':
            $article = $dbArticle->getResults($index);
            if(empty($article)) {
                $dbArticle->Add($index, $year, addcslashes($title), 'publication', $venue);
            }
            foreach($authors as $aut){
                if(!empty($aut))
                    $dbWrite->getResultsByAuthorArticle($aut, $index);
            }
            $i += 1;
            unset($authors);
            unset($title);
            unset($year);
            unset($venue);
            unset($ref);
            break;
    }
    #echo $i;
    #print_r($authors);
    /*if($i >= 2000) {
        print_r($venue);
        break;
    }*/
}
fclose($file_handle);
