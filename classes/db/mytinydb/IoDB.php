<?php
/**
 * Working with .dat files
 * Input/Output section
 */
namespace classes;

class IoDB {

    public static function read($name)
    {
        $myfile = fopen(BASE_PATH."/dat/".$name.".dat", "r") or die("Unable to open file!");
        $content = null;
        while(!feof($myfile)) {
            $content .= fgets($myfile);
        }
        fclose($myfile);
        $reader = json_decode($content);
        foreach($reader as $value)
        {
            $array[] = $value;
        }
        return $array;
    }

    public static function readline($name, $num)
    {
        $myfile = fopen(BASE_PATH."/dat/".$name.".dat", "r") or die("Unable to open file!");
        $content = null;
        fseek($myfile, $num, SEEK_SET);
        $content = fgets($myfile);
        fclose($myfile);
        foreach($content as $value)
        {
            $array[] = json_decode($value);
        }

        return $array;
    }

    public static function readbytes($name, $num, $lenght = 10000)
    {
        $myfile = BASE_PATH."/dat/".$name.".dat";
        $content = file_get_contents($myfile, NULL, NULL, $num, $lenght);
        return $content;
    }

    public static function write($name, $data){
        $myfile = BASE_PATH."/dat/".$name.".dat";
        $data = serialize($data);
        $data = strlen($data) . ' ' .$data;
        $handler = fopen($myfile, "r+");
        fseek($handler, 0, SEEK_SET);
        fwrite($handler, $data);
    }

    public static function writeline($name, $data){
        $myfile = BASE_PATH."/dat/".$name.".dat";
        $data = json_encode($data);

        $handler = fopen($myfile, "r+");
        if(filesize($myfile) < 100000)
            fseek($handler, 100000, SEEK_SET);
        else
            fseek($handler, filesize($myfile), SEEK_SET);
        fwrite($handler, $data);
        $pos = filesize($myfile) - strlen($data);
        return [$pos, strlen($data)];
    }

    public static function writebytes($name, $num, $data){
        $myfile = BASE_PATH."/dat/".$name.".dat";
        $handler = fopen($myfile, "r+");
        fseek($handler, $num, SEEK_SET);
        $data = serialize($data);
        fwrite($handler, $data);
    }


    /*private function json_encode($data)
    {
        return json_encode($data);
    }

    private function json_decode($json)
    {
        return json_decode($json, true);
    }*/
}