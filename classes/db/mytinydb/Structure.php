<?php
/**
 * Created by PhpStorm.
 * User: natalachilikina
 * Date: 20.11.15
 * Time: 12:48
 * Uses classes/btree
 */
namespace classes;

define('MIN_NUM_CHILDREN', 3);

require_once BASE_PATH . 'classes/btree/BTree.php';
require_once BASE_PATH . 'classes/db/mytinydb/IoDB.php';
use classes\BTree;
use classes\IoDB;

class Structure {
    private $tree;
    private $tables;

    function __construct($file){
        $tabs = array("p_articles", "p_authors", "p_venue", "p_write", "p_references", "p_keywords");
        $this->tree = $this->getMainData($file);

        if (empty($this->tree))
            $this->tree = new BTree();

        foreach($tabs as $tab){
            $$tab = $this->getTableParams($tab);
            $this->tables[$tab] = $this->getTableData($file, ${$tab}[0], ${$tab}[1]);
            if(empty($this->tables[$tab]))
                $this->tables[$tab] = new BTree();
        }
    }

    /**
     * Take to address of table
     * @param $table
     * @return BTreeData|null
     */
    function getTableParams($table){
        if(!$table) {
            return null;
        }
        return $this->tree->getData($table);
    }

    /**
     * Get the address of data from the table
     * @param $table
     * @param $data
     * @return null
     */
    function getDataParams($table, $data){
        if(!$table) {
            return null;
        }
        return $this->tables[$table]->getData($data);
    }

    /**
     * Get main data construct
     * @param $file
     * @return mixed
     */
    function getMainData($file){
        $data = IoDB::readbytes($file, 0, 10000);
        $array = explode(' ', $data);
        return unserialize($array[1]);
    }

    /**
     * Get the data from the table
     * @param $file
     * @param $pos
     * @param $length
     * @return mixed
     */
    function getTableData($file, $pos, $length){
        $data = IoDB::readbytes($file, $pos, $length);
        return unserialize($data);
    }

    /**
     * Update master-data of db
     * @param $file
     * @param $tree
     */
    function setMainData($file, $tree){
        IoDB::write($file, $tree);
    }

    /**
     * Update data in db
     * @param $file
     * @param $table
     * @param $data
     */
    function setData($file, $table, $data){
        $tab = $this->getTableParams($table);

        IoDB::writebytes($file, $tab[0], $data);
    }

    /**
     * Adding new tuple
     * @param $file
     * @param $table
     * @param $value
     */
    function addNewData($file, $table, $value){
        $IO = new IoDB;
        if(!$this->getDataParams($table, $value['id']))
            $pos = $IO->writeline($file, $value);

        $this->tables[$table]->insertData($value['id'], new BTreeData($pos));
        $this->setData($file, $table, $this->tables[$table]);
    }

}

