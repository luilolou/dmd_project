<?php
/*
 * Working with db
 */
namespace classes;

use PDO;
use Exception;

class TargetDB {

    /**
     * @var psql
     */
	private static $psql;
	protected static $_instance;  //экземпляр объекта

	public static function run()
	{

        if(self::$_instance === null){
			self::$_instance = new self();
		}

		return self::$_instance;
	}

    /**
     * Constructor
     *
     * @param $db
     */
    public function __construct($db)
    {
        $db_array = require_once BASE_PATH.'config/config.php';
        $db = $db_array['targetDB'];
        try{
			$this->psql = new PDO('pgsql:host='.$db['host'].';port='.$db['port'].';dbname='.$db['base'].';user='.$db['user'].';password='.$db['pass'].'');
		}
		catch(Exception $e){
			print_r($e);
			throw new Exception('Could not connect to the target DB');
		}

		$this->psql->prepare("SET NAMES UTF-8");
    }

	/**
	 * Magic method clone is empty to prevent duplication of connection
	 *
	 */
	private function __clone() {
	}

	/**
	 * Get connection
	 *
	 */
	public function getConnection() {
		return $this->psql;
	}

    /**
     * Making a query to the db
     *
     * @param null $query
	 * @return mixed
     */
    public function getResults($query = '')
    {
		$request = $this->psql->prepare($query);
		$request->execute();

		$error = $this->psql->errorInfo();

		if($error[1] != ''){
			throw new Exception('Error query PG '.$error[2]);
		}

		return $request;
	}


}