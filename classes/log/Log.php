<?php namespace classes;
define('ADMIN_EMAIL','chilikina.natalya@gmail.com');
class Log
{
	// Sending mail not very often

	static protected function send_to_admin($msg){
		try{
			$last_send = 0;
			if (file_exists('queue_last_send.txt')) {
				$last_send = file_get_contents('queue_last_send.txt');
			}

			if((intval($last_send) + 300) <= time()){
				$admin = ADMIN_EMAIL;
				mail($admin['email'], 'Ошибка на сайте', $msg);

				file_put_contents('/logs/queue_last_send.txt', time());
			}
		}
		catch(\Exception $e){
			$des=fopen("/logs/critical_log.txt",'a');
			fwrite($des, date('Y-m-d H:i:s')." Не отправляется сообщение о ошибке ($msg)\n\n\n\n\n\n");
			fclose($des);
		}
	}

	/**
	 * Critical problem, blocking all the program
	 * @param $msg
	 * @return bool
	 */
	static function critical($msg, $additional='')
	{
        echo __DIR__;

        if(!$msg){
			return false;
		}

		$date = date('Y-m-d H:i:s');
		$trace = var_export(debug_backtrace());
		$pid = getmypid();
		$pid_file = PID_FILE;
		$des=fopen("/logs/queue_log.txt",'a');
		fwrite($des,<<<TEXT
---------------------------------------
CRITICAL
$date
$msg
$additional

PID: $pid [$pid_file]
Trace:
$trace
---------------------------------------
TEXT
		);
		Log::send_to_admin($msg);
		fclose($des);
		return true;
	}

	/**
	 * Warning - not critical problem
	 * @param $msg
	 * @return bool
	 */
	static function warn($msg,$additional='')
	{
		if(!$msg){
			return false;
		}

		$date = date('Y-m-d H:i:s');
		$trace = var_export(debug_backtrace());
		$pid = getmypid();
		$pid_file = PID_FILE;
		$des=fopen("/logs/queue_log.txt",'a');
		fwrite($des,<<<TEXT
---------------------------------------
warning
$date
$msg
$additional

PID: $pid [$pid_file]
Trace:
$trace
---------------------------------------
TEXT
		);
		fclose($des);

		return true;
	}


	static function log($msg, $additional = 0)
	{
		if(!$msg){
			return false;
		}

		$date = date('Y-m-d H:i:s');
		$pid = getmypid();
		$pid_file = PID_FILE;
		$des=fopen("/logs/queue_log.txt",'a');
		fwrite($des, <<<TEXT
----
log
$date
$msg
$additional

PID: $pid [$pid_file]
----
TEXT
		);
		fclose($des);
		return true;
	}

	static function show_message($msg)
    {
		if(defined('DISPLAY_ERRORS')){
			echo $msg."\n";
		}
    }
}