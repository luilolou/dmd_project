<?
/**
 * Venue relations
 * Venues with names and id
 * int venue_id
 * string name
 * extends Core with all functions
 */

namespace classes;

class Venue extends Core
{
    private $table = "p_venue";

    public function getAll($sort = 'name', $order = 'asc')
    {
        return parent::getAll($this->table, $sort, $order);
    }

    public function getResults($id)
    {
        $condition = 'venue_id = '.$id;
        return parent::getResults($this->table, $condition)->fetch();
    }

    public function getResultsByName($name)
    {
        $name = trim($name);
        if($name == '') {
            return false;
        }
        $condition = "name = '".$name."'";

        $dbresult = parent::getResults($this->table, $condition);
        $arresult = $dbresult->fetch();

        if($arresult == null){
            $tmp = $this->Add($name);
            $dbresult = parent::getResults($this->table, $condition);
            $arresult = $dbresult->fetch();
        }

        return $arresult;
    }

    public function Add($value)
    {
        $value = "'".$value."'";
        return parent::Add($this->table, 'name', $value);
    }

    public function Update($id, $value)
    {
        $value = "'". $value . "'";
        return parent::Update($this->table, 'venue_id', $id, 'name', $value);
    }

    public function Delete($id)
    {
        return parent::Delete($this->table, 'venue_id', $id);
    }

}