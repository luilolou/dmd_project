<?
/**
 * Articles relations
 * int article_id
 * int year
 * string title
 * string type
 * int venue_id
 * extends Core with all functions
 */

namespace classes;

class Article extends Core
{
    private $table = "p_articles";

    public function getAll($sort = 'article_id', $order = 'asc')
    {
        return parent::getAll($this->table, $sort, $order);
    }

    public function getResults($id)
    {
        $condition = 'article_id = '.$id;
        return parent::getResults($this->table, $condition)->fetch();
    }

    public function searchTitle($s)
    {
        $condition = "title ~~* '%".$s."%'";
        return parent::getResults($this->table, $condition);
    }

    public function searchYear($s)
    {
        $s = intval($s);
        $condition = "year = ".$s;
        return parent::getResults($this->table, $condition);
    }

    public function Add($article_id, $year, $title, $type, $venue)
    {
        $value = "'". $article_id . "', '" . $year . "', '" .$title. "', '" .$type. "', '" .$venue. "'";
        return parent::Add($this->table, 'article_id, year, title, type, venue_id', $value);
    }

    public function Update($id, $name, $value)
    {
        $value = "'". $value . "'";
        return parent::Update($this->table, 'article_id', $id, $name, $value);
    }

    public function Delete($id)
    {
        return parent::Delete($this->table, 'article_id', $id);
    }

    public function getResultsByVenue($id)
    {
        $condition = 'venue_id = '.$id;
        return parent::getResults($this->table, $condition)->fetchAll();
    }

}