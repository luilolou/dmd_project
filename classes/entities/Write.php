<?
/**
 * Write relations
 * References from author_id to article_id
 * int author_id
 * int article_id
 * extends Core with all functions
 */

namespace classes;

class Write extends Core
{
    private $table = "p_write";

    public function getAll()
    {
        return parent::getAll($this->table);
    }

    public function getResultsByAuthor($id)
    {
        $condition = 'author_id = '.$id;
        return parent::getResults($this->table, $condition)->fetchAll();
    }

    public function getResultsByArticle($id)
    {
        $condition = 'article_id = '.$id;
        return parent::getResults($this->table, $condition)->fetchAll();
    }

    public function getResultsByAuthorArticle($author, $article)
    {
        $condition = "author_id = '" . $author . "' and article_id = '" . $article . "'";
        $dbresult = parent::getResults($this->table, $condition);
        $arresult = $dbresult->fetch();

        if($arresult == null){
            $tmp = $this->Add($author, $article);
            $dbresult = parent::getResults($this->table, $condition);
            $arresult = $dbresult->fetch();
        }

        return $arresult;
    }

    public function Add($author, $article)
    {
        $value = "'". $author . "', '" . $article . "'";
        return parent::Add($this->table, 'author_id, article_id', $value);
    }

    public function Update($id, $value)
    {
        return parent::Update($this->table, 'article_id', $id, 'name', $value);
    }

    public function Delete($id)
    {
        return parent::Delete($this->table, 'article_id', $id);
    }

}