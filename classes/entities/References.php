<?
/**
 * References relations
 * int article_id
 * int link
 * References from article_id to other articles (link)
 * extends Core with all functions
 */

namespace classes;

class References extends Core
{
    private $table = "p_references";

    public function getAll()
    {
        return parent::getAll($this->table);
    }

    public function getResults($id)
    {
        $condition = 'article_id = '.$id;
        return parent::getResults($this->table, $condition)->fetchAll();
    }

    public function Add($value)
    {
        return parent::Add($this->table, 'article_id', $value);
    }

    public function Update($id, $value)
    {
        return parent::Update($this->table, 'article_id', $id, 'link', $value);
    }

    public function Delete($id)
    {
        return parent::Delete($this->table, 'article_id', $id);
    }

}