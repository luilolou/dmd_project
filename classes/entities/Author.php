<?
/**
 * Authors relations
 * References from author_id to name
 * int author_id
 * string name
 * extends Core with all functions
 */

namespace classes;

class Author extends Core
{
    private $table = "p_authors";

    public function getAll($sort = 'name', $order = 'asc')
    {
        return parent::getAll($this->table, $sort, $order);
    }

    public function getResults($id)
    {
        $condition = 'author_id = '.$id;
        return parent::getResults($this->table, $condition)->fetch();
    }

    public function searchName($s)
    {
        $condition = "name ~~* '%".$s."%'";
        return parent::getResults($this->table, $condition);
    }

    public function getResultsByName($name)
    {
        $name = trim(addcslashes($name));
        if($name == '') {
            return false;
        }
        $condition = "name = '".$name."'";

        $dbresult = parent::getResults($this->table, $condition);
        $arresult = $dbresult->fetch();

        if($arresult == null){
            $tmp = $this->Add($name);
            $dbresult = parent::getResults($this->table, $condition);
            $arresult = $dbresult->fetch();
        }
        return $arresult;
    }

    public function Add($value)
    {
        $value = "'".addcslashes($value)."'";
        return parent::Add($this->table, 'name', $value);
    }

    public function Update($id, $value)
    {
        return parent::Update($this->table, 'author_id', $id, 'name', $value);
    }

    public function Delete($id)
    {
        return parent::Delete($this->table, 'author_id', $id);
    }

}