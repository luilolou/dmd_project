<?php
/**
 * Created by PhpStorm.
 * Core class with main functions for work with db
 * User: natalachilikina
 * Date: 28.09.15
 * Time: 5:21
 */

namespace classes;

require_once BASE_PATH.'classes/db/postgres/TargetDB.php';

class Core {
    private static $DB;
    private static $connection;

    public function __construct(){
        if(empty(self::$DB)) {
            self::$DB = TargetDB::run();
            self::$connection = self::$DB->getConnection();
        }
    }


    /**
     * Returns all tuples
     * @param $table Name of table to return
     * @return mixed
     * @throws \Exception
     */
    public function getAll($table, $order = 'id', $sort = 'desc')
    {
        $query = <<<SQL
			SELECT
				*
			FROM
				{$table}
		    ORDER BY {$order} {$sort}
		    LIMIT 50
SQL;
        $arrResult = self::$DB->getResults($query);
        return $arrResult;
    }

    /**
     * Returns all tuples satisfied the condition
     * @param $table Name of table to return
     * @return mixed
     * @throws \Exception
     */

    public function getResults($table, $condition)
    {
        $query = <<<SQL
			SELECT
				*
			FROM
				{$table}
			WHERE
				{$condition}
SQL;
        $arrResult = self::$DB->getResults($query);
        return $arrResult;
    }

    /**
     * Deletes a tuple from the table
     * @param $table Name of table to manipulate
     * @param $id_name Name of key column
     * @param $id Id of tuple to delete
     * @return mixed
     * @throws \Exception
     */
    public function Delete($table, $id_name, $id)
    {
        $query = <<<SQL
            DELETE FROM
				{$table}
			WHERE
              {$id_name} = {$id}
SQL;
        $arrResult = self::$DB->getResults($query);
        return $arrResult;
    }

    /**
     * Adds new tuple to the table
     * @param $table Name of table to manipulate
     * @param $params String with the params
     * @param $values String with values to add
     * @return mixed
     * @throws \Exception
     */
    public function Add($table, $params, $values)
    {

        $query = <<<SQL
            INSERT INTO
				{$table}
			({$params})
            VALUES ({$values})
SQL;
        print_r($query);
        $arrResult = self::$DB->getResults($query);
        return $arrResult;
    }

    /**
     * Update the tuple in the table
     * @param $table Name of table to manipulate
     * @param $id_name Name of key column
     * @param $id Id of tuple to update
     * @param $param String with the param
     * @param $value String with value to update
     * @return mixed
     * @throws \Exception
     */

    public function Update($table, $id_name, $id, $param, $value)
    {

        $query = <<<SQL
            UPDATE
				{$table}
			SET
                $param = {$value}
            WHERE
                {$id_name} = {$id}
SQL;
        $arrResult = self::$DB->getResults($query);
        return $arrResult;
    }

}