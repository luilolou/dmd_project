<?php
error_reporting(1);
define('BASE_PATH', __DIR__."/");
require_once BASE_PATH.'view/header.php';
require_once BASE_PATH.'view/nav.php';


require_once BASE_PATH.'classes/log/Log.php';
require_once BASE_PATH.'classes/entities/Core.php';
require_once BASE_PATH.'classes/entities/Author.php';
require_once BASE_PATH.'classes/entities/Article.php';
require_once BASE_PATH.'classes/entities/Venue.php';
require_once BASE_PATH.'classes/entities/Write.php';

use classes\Log;
use classes\Author;
use classes\Article;
use classes\Venue;
use classes\Write;

try{
    $article = new Article;
    $author = new Author;
    $venue = new Venue;
    $write = new Write;

    if(!empty($_REQUEST['searchform'])){
        $search = $_REQUEST['searchform'];

        $arrResult = $article->searchTitle($search);
        while ($result = $arrResult->fetch()) {
            $venuetmp = $venue->getResults($result['venue_id']);
            $result['venue_id'] = $venuetmp['name'];
            $writetmp = $write->getResultsByArticle($result['article_id']);
            foreach ($writetmp as $writer) {
                $tmp = $author->getResults($writer['author_id']);
                $result['author'][] = $tmp['name'];
            }
            unset($tmp);
            $arResult['title'][] = $result;
        }
        $arrResult = $article->searchYear($search);
        while ($result = $arrResult->fetch()) {
            $venuetmp = $venue->getResults($result['venue_id']);
            $result['venue_id'] = $venuetmp['name'];
            $writetmp = $write->getResultsByArticle($result['article_id']);
            foreach ($writetmp as $writer) {
                $tmp = $author->getResults($writer['author_id']);
                $result['author'][] = $tmp['name'];
            }
            unset($tmp);
            $arResult['year'][] = $result;
        }

        $arrResult = $author->searchName($search);
        while($result = $arrResult->fetch()) {
            $articles = $write->getResultsByAuthor($result['author_id']);
            foreach($articles as $art){
                $tmp[] = $article->getResults($art['article_id']);
            }
            $result['articles'] = $tmp;
            unset($tmp);
            $arResult['author'][] = $result;
        }


    }
}
catch(Exception $e){
    // пишем сообщение в лог
    Log::critical($e->getMessage(),var_export($e,true));
}

require_once BASE_PATH.'view/search.php';
require_once BASE_PATH.'view/footer.php';
