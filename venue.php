<?php
error_reporting(1);
define('BASE_PATH', __DIR__."/");
require_once BASE_PATH.'view/header.php';
require_once BASE_PATH.'view/nav.php';


require_once BASE_PATH.'classes/log/Log.php';
require_once BASE_PATH.'classes/entities/Core.php';
require_once BASE_PATH.'classes/entities/Author.php';
require_once BASE_PATH.'classes/entities/Article.php';
require_once BASE_PATH.'classes/entities/Venue.php';
require_once BASE_PATH.'classes/entities/Write.php';

use classes\Log;
use classes\Author;
use classes\Article;
use classes\Venue;
use classes\Write;

try{
    $article = new Article;
    $author = new Author;
    $venue = new Venue;
    $write = new Write;

    $arrResult = $venue->getAll();
    while($result = $arrResult->fetch()) {
        $articles = $article->getResultsByVenue($result['venue_id']);
        $result['articles'] = $articles;
        unset($articles);
        $arResult[] = $result;
    }
}
catch(Exception $e){
    // пишем сообщение в лог
    Log::critical($e->getMessage(),var_export($e,true));
}

require_once BASE_PATH.'view/venue.php';
require_once BASE_PATH.'view/footer.php';
