PGDMP             	        
    s            dmd_project    9.4.5    9.4.5 )    	           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            	           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            	           1262    578588    dmd_project    DATABASE     }   CREATE DATABASE dmd_project WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'ru_RU.UTF-8' LC_CTYPE = 'ru_RU.UTF-8';
    DROP DATABASE dmd_project;
             natalachilikina    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             natalachilikina    false            	           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  natalachilikina    false    5            	           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM natalachilikina;
GRANT ALL ON SCHEMA public TO natalachilikina;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  natalachilikina    false    5            �            3079    12123    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            	           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    182            �            1259    578624 
   p_articles    TABLE     �   CREATE TABLE p_articles (
    article_id integer NOT NULL,
    year integer,
    title text,
    type text,
    venue_id integer
);
    DROP TABLE public.p_articles;
       public         natalachilikina    false    5            �            1259    578622    p_articles_article_id_seq    SEQUENCE     {   CREATE SEQUENCE p_articles_article_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.p_articles_article_id_seq;
       public       natalachilikina    false    5    178            	           0    0    p_articles_article_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE p_articles_article_id_seq OWNED BY p_articles.article_id;
            public       natalachilikina    false    177            �            1259    578602 	   p_authors    TABLE     J   CREATE TABLE p_authors (
    author_id integer NOT NULL,
    name text
);
    DROP TABLE public.p_authors;
       public         natalachilikina    false    5            �            1259    578600    p_authors_author_id_seq    SEQUENCE     y   CREATE SEQUENCE p_authors_author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.p_authors_author_id_seq;
       public       natalachilikina    false    5    174            	           0    0    p_authors_author_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE p_authors_author_id_seq OWNED BY p_authors.author_id;
            public       natalachilikina    false    173            �            1259    578662 
   p_keywords    TABLE     C   CREATE TABLE p_keywords (
    article_id integer,
    word text
);
    DROP TABLE public.p_keywords;
       public         natalachilikina    false    5            �            1259    578651    p_references    TABLE     E   CREATE TABLE p_references (
    article_id integer,
    link text
);
     DROP TABLE public.p_references;
       public         natalachilikina    false    5            �            1259    578613    p_venue    TABLE     G   CREATE TABLE p_venue (
    venue_id integer NOT NULL,
    name text
);
    DROP TABLE public.p_venue;
       public         natalachilikina    false    5            �            1259    578611    p_venue_venue_id_seq    SEQUENCE     v   CREATE SEQUENCE p_venue_venue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.p_venue_venue_id_seq;
       public       natalachilikina    false    5    176            	           0    0    p_venue_venue_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE p_venue_venue_id_seq OWNED BY p_venue.venue_id;
            public       natalachilikina    false    175            �            1259    578638    p_write    TABLE     H   CREATE TABLE p_write (
    author_id integer,
    article_id integer
);
    DROP TABLE public.p_write;
       public         natalachilikina    false    5            �            1259    578350 
   view_index    VIEW     �  CREATE VIEW view_index AS
 SELECT n.nspname AS schema,
    t.relname AS "table",
    c.relname AS index,
    pg_get_indexdef(i.indexrelid) AS def
   FROM (((pg_class c
     JOIN pg_namespace n ON ((n.oid = c.relnamespace)))
     JOIN pg_index i ON ((i.indexrelid = c.oid)))
     JOIN pg_class t ON ((i.indrelid = t.oid)))
  WHERE (((c.relkind = 'i'::"char") AND (n.nspname <> ALL (ARRAY['pg_catalog'::name, 'pg_toast'::name]))) AND pg_table_is_visible(c.oid))
  ORDER BY n.nspname, t.relname, c.relname;
    DROP VIEW public.view_index;
       public       natalachilikina    false    5            �           2604    578627 
   article_id    DEFAULT     p   ALTER TABLE ONLY p_articles ALTER COLUMN article_id SET DEFAULT nextval('p_articles_article_id_seq'::regclass);
 D   ALTER TABLE public.p_articles ALTER COLUMN article_id DROP DEFAULT;
       public       natalachilikina    false    177    178    178            �           2604    578605 	   author_id    DEFAULT     l   ALTER TABLE ONLY p_authors ALTER COLUMN author_id SET DEFAULT nextval('p_authors_author_id_seq'::regclass);
 B   ALTER TABLE public.p_authors ALTER COLUMN author_id DROP DEFAULT;
       public       natalachilikina    false    174    173    174            �           2604    578616    venue_id    DEFAULT     f   ALTER TABLE ONLY p_venue ALTER COLUMN venue_id SET DEFAULT nextval('p_venue_venue_id_seq'::regclass);
 ?   ALTER TABLE public.p_venue ALTER COLUMN venue_id DROP DEFAULT;
       public       natalachilikina    false    175    176    176            	          0    578624 
   p_articles 
   TABLE DATA               F   COPY p_articles (article_id, year, title, type, venue_id) FROM stdin;
    public       natalachilikina    false    178   �+       	           0    0    p_articles_article_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('p_articles_article_id_seq', 1, false);
            public       natalachilikina    false    177            	          0    578602 	   p_authors 
   TABLE DATA               -   COPY p_authors (author_id, name) FROM stdin;
    public       natalachilikina    false    174   �2       	           0    0    p_authors_author_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('p_authors_author_id_seq', 531, true);
            public       natalachilikina    false    173            	          0    578662 
   p_keywords 
   TABLE DATA               /   COPY p_keywords (article_id, word) FROM stdin;
    public       natalachilikina    false    181   �I       	          0    578651    p_references 
   TABLE DATA               1   COPY p_references (article_id, link) FROM stdin;
    public       natalachilikina    false    180   �I       	          0    578613    p_venue 
   TABLE DATA               *   COPY p_venue (venue_id, name) FROM stdin;
    public       natalachilikina    false    176   �I       	           0    0    p_venue_venue_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('p_venue_venue_id_seq', 112, true);
            public       natalachilikina    false    175            	          0    578638    p_write 
   TABLE DATA               1   COPY p_write (author_id, article_id) FROM stdin;
    public       natalachilikina    false    179   �O       �           2606    578632    p_articles_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY p_articles
    ADD CONSTRAINT p_articles_pkey PRIMARY KEY (article_id);
 D   ALTER TABLE ONLY public.p_articles DROP CONSTRAINT p_articles_pkey;
       public         natalachilikina    false    178    178            �           2606    578610    p_authors_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY p_authors
    ADD CONSTRAINT p_authors_pkey PRIMARY KEY (author_id);
 B   ALTER TABLE ONLY public.p_authors DROP CONSTRAINT p_authors_pkey;
       public         natalachilikina    false    174    174            �           2606    578621    p_venue_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY p_venue
    ADD CONSTRAINT p_venue_pkey PRIMARY KEY (venue_id);
 >   ALTER TABLE ONLY public.p_venue DROP CONSTRAINT p_venue_pkey;
       public         natalachilikina    false    176    176            �           2606    578633    p_articles_venue_id_fkey    FK CONSTRAINT     }   ALTER TABLE ONLY p_articles
    ADD CONSTRAINT p_articles_venue_id_fkey FOREIGN KEY (venue_id) REFERENCES p_venue(venue_id);
 M   ALTER TABLE ONLY public.p_articles DROP CONSTRAINT p_articles_venue_id_fkey;
       public       natalachilikina    false    176    2186    178            �           2606    578668    p_keywords_article_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY p_keywords
    ADD CONSTRAINT p_keywords_article_id_fkey FOREIGN KEY (article_id) REFERENCES p_articles(article_id);
 O   ALTER TABLE ONLY public.p_keywords DROP CONSTRAINT p_keywords_article_id_fkey;
       public       natalachilikina    false    181    178    2188            �           2606    578657    p_references_article_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY p_references
    ADD CONSTRAINT p_references_article_id_fkey FOREIGN KEY (article_id) REFERENCES p_articles(article_id);
 S   ALTER TABLE ONLY public.p_references DROP CONSTRAINT p_references_article_id_fkey;
       public       natalachilikina    false    180    178    2188            �           2606    578646    p_write_article_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY p_write
    ADD CONSTRAINT p_write_article_id_fkey FOREIGN KEY (article_id) REFERENCES p_articles(article_id);
 I   ALTER TABLE ONLY public.p_write DROP CONSTRAINT p_write_article_id_fkey;
       public       natalachilikina    false    178    2188    179            �           2606    578641    p_write_author_id_fkey    FK CONSTRAINT     |   ALTER TABLE ONLY p_write
    ADD CONSTRAINT p_write_author_id_fkey FOREIGN KEY (author_id) REFERENCES p_authors(author_id);
 H   ALTER TABLE ONLY public.p_write DROP CONSTRAINT p_write_author_id_fkey;
       public       natalachilikina    false    179    174    2184            	   �  x��X[W�H~�_�O{v��A��a�����CJ�5Ig�;�̯��KBLq�I}]�﫪�u����ٗۿ�>�{I�/
�%KW���ք�d��D�|�Alɐf���Lm?e�c�"�O����mw/h*i�%S��$�*��)�[RE�n��Dz0�Ϋ���?�2�Bg�t�� � :��~�m\t�3��KX���4�$\�<�N�@)�d@��)�=r��K�፩�����u�Q={ԈI%�cnp���]i�S�$T\l/�6�LW��Hy����P���2z�g��!��75)��L�'.l�r	���<�5�=�FkH(�h�z�cb]�R~��4m���ײx��V��q���|�&�|�[�)N�T(\�;xQ��"*�*�s�:�1_�L
��Pc�%��0��	Y ժ8A�,�u�S��v��uYq�?ኄ�����o�'p:�����F[��(�Ϡ�V�
���z�\�=g��r��9�#�H}�I�*�ͻC��O镾ժ�,�´�}Xr?�S�=�o>�$�q^$�pIgf���`y�B!���i�R[�za�o�1�՚��r� |��o�`�W����k9IT�/���$(Km��8 7iC����KT��j�:�c�ej���"��^��_ �u��*=]����S�4�]p�����M�j������/!�Ɖz�>�c��+s�G�N�<��s�����3�ڎ����B��@�rh�N:�IF�<���\fӛ�����=ǣ�'l����l�����L
����$����h�k�혧�iC�5~V|<�tJ���.|[�.}|ͧ^��+�S>�ǚ�ԧgu�ՠ:���=���s
� ꈶ=k�G:~Zە��F�oԷFR])^MIa��a��U4~>������Z�cSd1�H��Ec�e�*�@�U�C�8����p��S���ʜ�BL��'4�h9Wj��Yk6 ߢm-�����dvw���h�m�.�E�֌c���7gZC$�4���"Y��-'����^,��5�
W1���Ɨ�`DYV@4���9�K�O�	t(j\��>@oGm�	
�)�e�J�$1
�=����}�~�nĢ��.u�h����2�Vv��p;g�.�\l`��P�uA�m�h�W� ��'4�FZ8�b�
lK��$������S�� �.��x�H����8k�r�?ݖ=I����뜗YXS��~u2ҏX �:���fH�x �q�|�ul#�b�
�Pie�lL�`7�fk5�խ�>5 9R�pu����w_�ص}�^�<.����c��b;Lv�i8�`�_0-WϾkX�-5"����f1�8c���u)v���5��W;X�Ŋ�{�R����eݒm��ʗ�M����[�m�mÝ�0`��|�5�-�I��f\�b��hs�cu,փ�ښ��F������'����֕��%�q\ɫe�^�N�����9��ۀ�s�d*���OL�i�ޣA�,��0�����ۻutƛ�?b�Vh�wV���X6�+�%�v�(��y1���/7L2z"�ř��ۣ��Bn`L8�41�^�׭x�a�4�ҕ{��R���R�m}���a�O�o�㢵K�=K���Ā�x��0��p��[�Fh[��w3�����m�+�D��-i�VN�5��W�>����      	      x�U��n�H����S�ʰDR���n��c��Ѝ٤���%����n�A��1?f0�^Ы����;IɮhtY!2/�眈�0z�M;���� �¿i���EW�y���� 9.�Җv��_��%ǵu����x]�Y�����3g����U&�FGϵ)�*yt峭�Ya�m<8���*99H��6�`=���MrRԮi��w���j�X_��i�k�j=�4�ee����ɩ٘2d�Y�f��Ari��W� ��lU�['��ܯ��(:��tK�@�69um�KE<Gץ�����$����U�e�Ńitg��TɅ����ata\��$��5�����+K�n�m)7��cW�6�>�1���i��Wɵ[��,:���M.���a�/�b�v��p��c�ɭ��ҷ������;K:)���,����qVX�j�x8��M�5]�5qz}u��Vmr�:��)����Atj^�<���]0W:���*9��}k�T�P��h���j�4��Y�qɭ�q�G'�]iا���.Nq����}[�8Ggs�g���rv�0+�=H�̊3y��-^M�ځ]&]㪥�on���C�&��ca�"�Ǌ�y���a�͔��� 9��K��ѭ�_N
gy�S��,z0/.9*VvgD�Ӟ�8i++W%�]��83��M�N]+gg���/j���������|�Cf)�Eró���8D'�q��;-�j�`̇�ޜ�O����UW���9�Q�8�+[ˣ�ۓ(�r�-�<ߝԭq�S����*��e'�8:=H�s$�D�V4�EAх�2aaf��%ZWţ��LyR�r��6�ģAt�v�e�W���i���r���o$K<�:�`�!my�Gg��VVOr�Kךx4�N=�w�w��g^+��q<��$%�h�/�[W�lJ�1?�,:c�M<>�݊�3��{~Ƣ��k�<H�]S(M���#'*��R�g�x��ck�n��#�i���'��|ͨ�߁����1c~y.M��'���^M�GF��!Ǔ����po6�������d��|��x��x2T�;�EkK�J��|��x��'$�k_�W�f��Mr�Qrl��w�d]z����cS׀P�a�It��B��j�g2��6�ѩ)m<%�=h����t���[A��7��f-.de�FOӨ�6o�4�y�3���r��ug��,U6i:�~nf���)H�m<e��&yjp]��-��N�� |  ��K<�����]��P�Q����9|��:�p|���p8���z��8K�)F��x��Z���Y��G��:���0� �;���m�'p�����F�`S��4���������bp8�U�/���ǿ6�Lxݘ��X�o��#���©K�3���(z�M7w�8��L���;Yt����o�\�9��-�ˋ�Y����sR�~�1����'Oݶp�3>�]������B�b*U:�v�ZT}�?滓�!H��)\χs���(���\��%��fŋ�u�<���T:� �ت�2�<�����AF��ڗ�T'k���͒�$�O���o!9t	���f ��`|4���*�y疈�#=�Y�� K���cW���o��cS��]Wn&[&?��`�&�3�H�9UVW�Ҕ�PX��#9�����c�rY�MY�,M���%8��n�p�P�`=*�4�p?�n����L��ܸ�M��^���#f�L ����mI=,��e!49{�u)������:8���L
��
@���vN�y�M�g�!���0�q�6�o*�o���.CC�絩��vӽ�ؒS�?X�H�	��߳�FasN���Կ{�@I"�Pkazȗ*�6�c��u�~F|����P���3�I"�3̕�>W���3�藷���U�O���Im���dg[`��a��ҕ`2�{!�vյ��F���O	p���:L��aʀߠ���T�Q��u�x�`ܛT3�{����6A1��%�����c�Շ�=*�R�[�3ǥ��i�O��J)	$�F��ꨙ��C���Th��%��7��`����{�.���5���(��|gm	��R�]�g��/r��+����7�bn6
; G�����=�A ܹ�����^�aO�`���#�[�B�`q��5>?a�\��ڽo�{HV�0�OĽ�cc4�X���/�s��a����P5xQ��R�6U"?X+�!���_�p0�Fg�_��y�!2��+Yx��N�1����Z�R����$G�X;��<�}S$aBI�Mxw$'}�U&�0�A�@w��k�I�:�,򥰥^����*�uC1Ŏ��Y�Wj�h=����an�}I�����}���C٧����(��}�A��}��<�*�[i�q 1�؍��;e��p����/���0xf
�ue ����� �Q�
�R5W3���@H�顒*r�߽��0.�P�۽�	Ô+P��Q�ޗ��}rN�hg�&S9���s����8B�I���ʅ�1*�9�B�"1�6��+6�ճH���{��\Rs��Gu5o����1C���W	GD�Nc���
�����|b{[އ������VO�Z	� �>g"+��A"ص��~k��R��z��K��9��;~����� ɍ�b�b�0��\X�ǔ��/�J����ѯ��r�Q�z]�m��*h��R�;�愻��,���ri�c��(Ȅ!,.p_�i��J�!,���U�h�״m�L�9a�_9ѩ���0L�� YB5XU�:��r�<��Jz%4�>��Eݽ�`�Sd�:�o���Ei��rk���"�?toa�|��t��!���d�WY�j�Od��gPֽ��Mv����ک�B����CIYz���m��N�X׈Y��!t�������.N��ı��r�qQ�5������Y��1̳����0�e��R�u�+� z��^@g�ks��	����/D�Jm��Vzh�ᖯ@}��x׃��BIu��i���ga��e���y���W��w�UhQj��A.;�Ԉy�aJ������*��^��$TP��G���H:r|L<���!�� ����O�'F;sg� ��#��#M'�2�������k1ߺE�HB\��]�D��?_�+��̓��l���0RY�	��[�fǐ���S��l�\�я� �f\8]$��]̈́�K"���"ȫ�xMh��j��`�7ꦩ��Ȁr�����ҋ���2�b��6��Xb�I�)!`H R����� ՠ���KG�;��ɪ�W�G���ԵV�D<�ZEk rۄDG���FT�<	���(��p9[!E���U	��軆p�~.�l�{g��xV��֫�G	pn�g�g6Q���1V��*�!������P�՟�}m�/��#	�"N� �{dG���@�<��ts` =ܗ��Ԏ�\=�I Ձ(k��#s�C��jY�蠦k�&�e�Z��%j��!�/����/G!L�m�x�Kܧ'U�43
���ew5�ItVj�֬=tf>w�|�T޾�_$�<NU��ж�,�y�/ѭU���<�~5��PT��`b辏�cP���r���#q�C&kx!)ܾ�TVF,Z;	�|���#¾	W�֒)�.1�ʫ}T�!t�R��A�"#7������	f(��bP8-�ޝ��>
E���>N9����mm�F�+��.�#)������܄�*(�yۄ-B�h����K/��B�Oҿ�o�}��y�_���!8��Ҵo��`���z�7��^���!ΖA�^�!�B�Gb�]ɑ�2/�l�{�c y�8+��W��Y+B���\hC&X��M8�IL�tY�>3��G���I��*��L�UMX�Օ�7+���@:a�v*\��$Ɔ�j5-�,�;�ʴ���>� �	�����
�Q�_�~�ϴb(�
��jͳ�WS��C�(N�t
�j��jݩ`s�m*F�|C��-����'��R�D�EB�Z��}�2���,V]�!)�M�Dz��ܮ�@��a����'V�4�P'
������|�ox�� �p
^�*!VVU~��9��s�b�0��J��~�g5�+�F�Tn�d
C��*�� �  "��b`LE��Y�_[�R
C�,������k�c�X�*��qk����?���]��5�u���z�����x�Uzj����rL!�=�p�l^��Oo@�V1�b�(��h�&���Fxɚ�����?ơ!�"���̂Ab��.��5T�V�hNa�c���,������h�Y
��W��)~�g�U�־���4��Z�����E��]k�s\��0�C���>�!�Z��>j0�	�W���uzyk�k�e����O��(�3�I9�������Ma�K��ON��L�SQk�ӝ�Z`eU���^���[�Ǧ�� d�?����,H̞�S��X:T�<b�)\4���c���+5�d����ū�7��e���C�H��W�Gk?7��� ����x���Q�3}v��'��L���u��A��[�~�F�?.�$ܓA��֭�s�h0I�W����*M2�y�R��Pv�+QF�0��]��(��UQ��J�L�ꪇ��+�b�@U�%0�ø�]A�y(>��'�[�U��}�C1�)2��,�. ���+�0�8��Ԯ9	��a	����x>�W�K�¬�J�����Q�Ò�Ƞ�pI'v8�`�y�����[�[Y��q֊���A�L�5���>��`9�ֺ4�t!mk�
���X`����j/0�c��f�9<���p+_L�!q���UW��,�gG�O�A�GON��4[>C	P0d��ʠ�.���W�,�=�So��J��|:!aj�-	�V�J��KN��������@��o�8��Pe�t&�>���ʛ���,��:�@���$S�!P�t5Y-I�L���6�y�8�L?ب��Ԡ&�2�h���B-鮷-t6�� P��ӡ�P�[FJ=;��
7 �tZ�M{�������u�Ǔ�ªE�Aӷ����2[��eR3�R��@uj^ep��Em�:��Е���3]/j�Ka��>���ũ}��J
��tgPM�[��"�Ե��W���:N�j�e0�7�����FM_��T ��8���_Pn*VS��Su�8g��C�z;e�K�j�Į�M��.+�������_��)�_Z��3)B6���&�
����@&Q�M���a�Wn�	g���	�����Ͼ6��OG0M�t�6v��=Bq��I��s-���:N�GT�އ��j�e���Y6�ʩ�ϡb��@��!Za�,���^�9L�R�~$2PEׯF�����K�R��nɲ�p_��:P���&�KY]%�����p4P�dyy?��z~�~�R7��d��(֕��.c�0�<(���Ɖ~G�tJ6��΍҉$.���Y �25�u�t���'��d�����m���q����947�����fp��nF�|�j��	{�t�tO�G��Н���RQw;��7�@�q���ֆ���0t7tt�[i8L�ʀ�z۝�����:��m����a�c�@�n����0�%!xg�����U�j-z�8��磢�C�v�Um[m�J=�Z��8�� !��٦�ӝ���e������s�C
���`��S���t�$��-�`u��ǻD�#���a�8�$��{ސ�ܥؗ3Wz�6�zo��8��hȡ� .C���r�|ߖ}��	�8��T�����Bq>1�ƀ�������j�I�vM��a>����\-]u�r]�3x7��e�By�~�!�'f���>u�~���KgAb�zl��z�#�$�s�:�n&�k�������{1�4���= �������Ϯ4�t���U��q�?p��      	      x������ � �      	      x�3�4�2�4bS 6����� ��      	   �  x��VKs�6>���3{D�E�d�n5�+��\z��5�4 hG��]���rډo���o�]|���m��Ԟ��B�Z����g��oy	��U����)�B��N�d������r�����{m���dF@8c��M���$��]�ÖP�\�?0�V�'���*�Jk�k��bHwJK�zA��q�b?)��/U.�*�9��b}�b��}e��j�ņr������R�Q?�{�s�4	ɂ[#����ԖL`Q;�,"$���aD�ö_�9[J��2�t�ʾ��IVX�3fD�ӹzD@�h8�~s���r��~�ɪ�K(pt�`^+Elh8#��Ǡ�G��g=���*p��K��6�H���\dй�N��C��o�%����<��( �:i�U���rs���Ng�/�ޡ��*�ڪ����h-K������M}����7-�����e�����.řJ�!쁝�?�(���]�:Y�]�Ѧ%;�����k�-���+��D�)Y�*��TC�,�W�RY7��?֑i��ʖM���,�4<��E��[G�<��MU�f�+EZ/�)E�%����D<���X�a���N+GAVp��O:��3.\���4 �*=�R�!�u�_������d����?択�y����r�\�胾�:=rm�lm[���4&�=~�tFn*iE&p2%����~]�N���v#��DU�xD�TH<	�eC�椕l�)�poH}���g��<QVeYa�8 ��#n��n]0m����M��v���~L�ɯ�Oh��E�vuJ��a?��/`�e�)Y��_V4����b���\�[����k��B�z�o������8!_�~���Ш�+�o�ǝ����t6"WF���F��Ǵ�������f����[�c�AꮍPx9oڽwi4�Ag����'t;���a����.��.���8(-[`���,������?+ߴI�RX6v�ָsx�f�v٬�^�Y+X�V`-��,&��V.G��@U�\�.Ww�kCCx��}_�\	L��Q�f�����Sи���Y!�Ε�����D��WB��7*�ќzåIHތ�'C��9?�D�d�ɴGg|�R���wB�x��{������J2�������"�u�a�g{R��inf4��o�۰C��\TBf5��Ѓ�Slm56���n�ƞϸ�n���#M:4��Ow��P&��&6���ā��dN�祭v�#�(Ȍ]��<��9M�e����5BGL/s��|��1
ź�S'�\2��ɓ`���iJ;��E�7;�PILq�n3]�0A���>����v�X�HOpct���ۂ�v����` ����5�NƟ0Ԙ�s��s�BZBZI^�]:	��QGH��3J�ch��      	   p  x�=��m�0�3YL ��^�!GF�F��1ls������z~E�r�`��(Y|�%�Xj�tY@S���*$ª��i۵� Qքs֠À�ZPᅳ_�g}q��6��&��M��
��*[�$5�"u�K��5i�8/�©iv��wu��m��qȔC��@�J���8&,xa�y����rtp�m��B���&��|>�B�&,xW�^�h�h�hpx�3��3&s����z�:�{��}jO�i.n���r���N����P��b�qGS_���=�b*03�JgX�̔�9�����3!�!/���"_L��N�ؗ��xn}~9�u�1���Ѯ�c�ܿ�w_�}��7g������� �?�1     