<?php
error_reporting(1);
define('BASE_PATH', __DIR__."/");
require_once BASE_PATH.'view/header.php';
require_once BASE_PATH.'view/nav.php';


require_once BASE_PATH.'classes/log/Log.php';
require_once BASE_PATH.'classes/entities/Core.php';
require_once BASE_PATH.'classes/entities/Author.php';
require_once BASE_PATH.'classes/entities/Article.php';
require_once BASE_PATH.'classes/entities/Venue.php';
require_once BASE_PATH.'classes/entities/Write.php';
require_once BASE_PATH.'classes/entities/References.php';

use classes\Log;
use classes\Author;
use classes\Article;
use classes\Venue;
use classes\Write;
use classes\References;

try{
    $article = new Article;
    $author = new Author;
    $venue = new Venue;
    $write = new Write;
    $reference = new References;


    if(!empty($_REQUEST['id']) && intval($_REQUEST['id'])==$_REQUEST['id']){
        $id = $_REQUEST['id'];
        if(!empty($_REQUEST['updatebook'])){
            foreach($_POST as $pid => $post) {
                if(($pid == 'title') || $pid == 'year'){
                    $result = $article->Update($id, $pid, $post);
                }
                if(($pid == 'venue_name')){
                    $result = $venue->Update($_POST['venue_id'], $post);
                }
            }
        }
        $result = $article->getResults($id);
        $venuetmp = $venue->getResults($result['venue_id']);
        $result['venue_name'] = $venuetmp['name'];
        $writetmp = $write->getResultsByArticle($result['article_id']);
        foreach ($writetmp as $writer) {
            $tmp = $author->getResults($writer['author_id']);
            $result['author'][] = $tmp['name'];
        }
        unset($tmp);
        $refs = $reference->getResults($id);
        foreach($refs as $ref) {
            //print_r($ref);
            $result2 = $article->getResults($ref['link']);
            $venuetmp2 = $venue->getResults($result2['venue_id']);
            $result2['venue_name'] = $venuetmp2['name'];
            $writetmp2 = $write->getResultsByArticle($result2['article_id']);
            foreach ($writetmp2 as $writer) {
                $tmp = $author->getResults($writer['author_id']);
                $result2['author'][] = $tmp['name'];
            }
            $result["refs"][] = $result2;
        }
        unset($tmp);

        $arResult = $result;
    }
    else {
        $arrResult = $article->getAll();
        while ($result = $arrResult->fetch()) {
            $venuetmp = $venue->getResults($result['venue_id']);
            $result['venue_id'] = $venuetmp['name'];
            $writetmp = $write->getResultsByArticle($result['article_id']);
            foreach ($writetmp as $writer) {
                $tmp = $author->getResults($writer['author_id']);
                $result['author'][] = $tmp['name'];
            }
            unset($tmp);
            $arResult[] = $result;
        }
    }
}
catch(Exception $e){
    // пишем сообщение в лог
    Log::critical($e->getMessage(),var_export($e,true));
}

require_once BASE_PATH.'view/index.php';
require_once BASE_PATH.'view/footer.php';
