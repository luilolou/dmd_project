<?
#print_r($arResult);
?>

<div class="container-fluid">
    <div class="row">

        <?require_once 'leftnav.php'; ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Articles</h1>

            <?if(!empty($id)):?>
                <div class="table-responsive">
                    <form method="post" action="/index.php?id=<?=$arResult['article_id']?>">
                        <input type="hidden" id="updatebook" name="updatebook" value="yes">
                        <input type="hidden" id="id" name="id" value="<?=$arResult['article_id']?>">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Field</th>
                            <th>Value</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr><td width="100px">#</td><td><?=$arResult['article_id']?></td></tr>
                            <tr><td>Title</td><td><input id="title" name="title" size="120" type="text" value="<?=$arResult['title']?>" /></td></tr>
                            <tr><td>Authors</td><td><?foreach($arResult['author'] as $aid => $author):?>
                                        <input id="author<?=$aid?>" name="author<?=$aid?>" size="120" type="text" value="<?=$author?>"/><br/>
                                    <?endforeach;?></td></tr>
                            <tr><td>Year</td><td><input id="year" name="year" size="120" type="text" value="<?=$arResult['year']?>"/></td></tr>
                            <input id="venue_id" name="venue_id" size="120" type="hidden" value="<?=$arResult['venue_id']?>"/>
                            <tr><td>Venue</td><td><input id="venue_name" name="venue_name" size="120" type="text" value="<?=$arResult['venue_name']?>"/></td></tr>
                        </tbody>
                    </table><input type="submit" id="send" name="send" value="Send"/>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Authors</th>
                            <th>Year</th>
                            <th>Venue</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?foreach($arResult['refs'] as $res):?>
                            <tr>
                                <td><?=$res['article_id']?></td>
                                <td><a href="?id=<?=$res['article_id']?>"><?=$res['title']?></a></td>
                                <td><?foreach($res['author'] as $author):?>
                                        <?=$author?><br/>
                                    <?endforeach;?>
                                </td>
                                <td><?=$res['year']?></td>
                                <td><?=$res['venue_id']?></td>
                            </tr>
                        <?endforeach;?>
                        </tbody>
                    </table>
                </div>
            <?else:?>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Authors</th>
                            <th>Year</th>
                            <th>Venue</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?foreach($arResult as $res):?>
                            <tr>
                                <td><?=$res['article_id']?></td>
                                <td><a href="?id=<?=$res['article_id']?>"><?=$res['title']?></a></td>
                                <td><?foreach($res['author'] as $author):?>
                                    <?=$author?><br/>
                                    <?endforeach;?>
                                </td>
                                <td><?=$res['year']?></td>
                                <td><?=$res['venue_id']?></td>
                            </tr>
                        <?endforeach;?>
                        </tbody>
                    </table>
                </div>
            <?endif;?>


        </div>
    </div>
</div>
