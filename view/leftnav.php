<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li <?if(strstr($_SERVER['REQUEST_URI'], 'index.php')):?>class="active"<?endif;?>><a href="/index.php">Articles <span class="sr-only">(current)</span></a></li>
        <li <?if(strstr($_SERVER['REQUEST_URI'], 'author.php')):?>class="active"<?endif;?>><a href="/author.php">Authors</a></li>
        <li <?if(strstr($_SERVER['REQUEST_URI'], 'venue.php')):?>class="active"<?endif;?>><a href="/venue.php">Venues</a></li>
    </ul>
</div>