<?
#print_r($arResult);
?>

<div class="container-fluid">
    <div class="row">

        <?require_once 'leftnav.php'; ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Authors</h1>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Authors</th>
                            <th>Articles</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?foreach($arResult as $res):?>
                            <tr>
                                <td><?=$res['author_id']?></td>
                                <td><?=$res['name']?></a></td>
                                <td><?foreach($res['articles'] as $art):?>
                                    <?=$art['title']?> // <?=$art['year']?><br/>
                                    <?endforeach;?>
                                </td>
                            </tr>
                        <?endforeach;?>
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>
