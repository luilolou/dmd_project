<?
//print_r($arResult);
?>

<div class="container-fluid">
    <div class="row">

        <?require_once 'leftnav.php'; ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Articles</h1>



            <h2 class="sub-header">By title match</h2>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Authors</th>
                        <th>Year</th>
                        <th>Venue</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?foreach($arResult['title'] as $res):?>
                        <tr>
                            <td><?=$res['article_id']?></td>
                            <td><a href="?id=<?=$res['article_id']?>"><?=$res['title']?></a></td>
                            <td><?foreach($res['author'] as $author):?>
                                    <?=$author?><br/>
                                <?endforeach;?>
                            </td>
                            <td><?=$res['year']?></td>
                            <td><?=$res['venue_id']?></td>
                        </tr>
                    <?endforeach;?>
                    </tbody>
                </table>
            </div>
            <h2 class="sub-header">By year match</h2>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Authors</th>
                        <th>Year</th>
                        <th>Venue</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?foreach($arResult['year'] as $res):?>
                        <tr>
                            <td><?=$res['article_id']?></td>
                            <td><a href="?id=<?=$res['article_id']?>"><?=$res['title']?></a></td>
                            <td><?foreach($res['author'] as $author):?>
                                    <?=$author?><br/>
                                <?endforeach;?>
                            </td>
                            <td><?=$res['year']?></td>
                            <td><?=$res['venue_id']?></td>
                        </tr>
                    <?endforeach;?>
                    </tbody>
                </table>
            </div>


            <h2 class="sub-header">By author match</h2>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Authors</th>
                        <th>Articles</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?foreach($arResult['author'] as $res):?>
                        <tr>
                            <td><?=$res['author_id']?></td>
                            <td><?=$res['name']?></a></td>
                            <td><?foreach($res['articles'] as $art):?>
                                    <?=$art['title']?> // <?=$art['year']?><br/>
                                <?endforeach;?>
                            </td>
                        </tr>
                    <?endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
